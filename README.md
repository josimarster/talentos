# Skidun Talentos
---
Venha fazer parte da Skidun, uma agência digital hiperativa! Você irá fazer parte do time de tecnologia, que desenvolve diversos projetos para clientes como Infoglobo, Accor, Globo.com, Petrobrás e TV Globo. Nosso desafio é trabalhar com o que há de mais moderno em desenvolvimento para web! Aplicativos, sites, hotsites e sistemas serão produzidos diariamente, utilizando tecnologias como HTML5, CSS3, AngularJS, Node.JS, PHP, Wordpress, entre outros!

### Se você chegou até aqui, essa vaga pode ser sua! Veja as instruções para enviar seu currículo na branch 'curriculos' :D
